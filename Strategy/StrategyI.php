<?php

namespace Strategy;

interface StrategyI
{
  public function execute(int $number1, int $number2);
}
