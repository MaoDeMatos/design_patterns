<?php

namespace Command;

interface CommandI
{
  public function execute();
}
